import { writable } from "svelte/store";

export const hasImage = writable(false);
export const glitchSeed = writable(25);
export const glitchQuality = writable(30);
export const glitchAmount = writable(35);
export const glitchIterations = writable(20);
